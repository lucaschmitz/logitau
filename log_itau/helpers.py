# Arquivo responsável pelas funções auxiliares do projeto

from datetime import datetime
from .models import LogDocumento
from .log_settings import logger
from .database import cursor, conn


def instanciar_arquivo(entrada, identificador):
    """ Função responsável pelo instanciamento do objeto dado o valor de entrada """

    saida = LogDocumento(
        dt_register=datetime.now(),
        date=entrada[0],
        time=entrada[1],
        site_name=entrada[2],
        computer_name=entrada[3],
        s_ip=entrada[4],
        method=entrada[5],
        uri_stem=entrada[6],
        uri_query=entrada[7],
        port=entrada[8],
        username=entrada[9],
        c_ip=entrada[10],
        version=entrada[11],
        user_agent=entrada[12],
        cookie=entrada[13],
        referer=entrada[14],
        status=entrada[15],
        sub_status=entrada[16],
        win32_status=entrada[17],
        time_taken=entrada[18],
        identificador=identificador)
    return saida


def objeto_para_tupla(entrada):
    """ Função responsável em transformar o objeto em tupla, pois tupla é o formato qu o pyodbc aceita para insert """

    saida = (
        entrada.dt_register,
        entrada.date,
        entrada.time,
        entrada.site_name,
        entrada.computer_name,
        entrada.s_ip,
        entrada.method,
        entrada.uri_stem,
        entrada.uri_query,
        entrada.port,
        entrada.username,
        entrada.c_ip,
        entrada.version,
        entrada.user_agent,
        entrada.cookie,
        entrada.referer,
        entrada.status,
        entrada.sub_status,
        entrada.win32_status,
        entrada.time_taken,
        entrada.identificador)
    return saida


def inserir_dados(entrada, tabela):
    """ Função responsável por inserir os dados na tabela stage [VALIDACAO] """

    # Query SQL para inserção dos dados no Banco de Dados
    query = """INSERT INTO [dbo].[VALIDACAO] (
                    [DT_REGISTER] 
                ,   [DATE] 
                ,   [TIME] 
                ,   [S_SITENAME] 
                ,   [S_COMPUTERNAME] 
                ,   [S_IP]
                ,   [CS_METHOD]
                ,   [CS_URI-STEM]
                ,   [CS_URI-QUERY]
                ,   [S_PORT]
                ,   [CS_USERNAME]
                ,   [C_IP]
                ,   [CS_VERSION]
                ,   [CS_USER-AGENT]
                ,   [CS_COOKIE]
                ,   [CS_REFERER]
                ,   [SC_STATUS]
                ,   [SC_SUBSTATUS]
                ,   [SC_WIN32-STATUS]
                ,   [TIME_TAKEN]
                ,   [IDENTIFICADOR]) 
                VALUES
                (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)"""

    # Função do pyodb para uma inserção rápidas de dados em grande escala (obs.: Não impacta na integridade do dado)
    cursor.fast_executemany = True

    # Manuseio de erro esperado, pois algumas vezes o arquivo pode não ter um método especifico,
    # e com isso não passa parametro para inserção
    try:
        cursor.executemany(query, entrada)
    except Exception:
        logger.warning('Tentativa de insercao de dados, porem a lista de entrada esta vazia, '
                       'procedimento normal, pois as vezes nao existem dados de um metodo especifico para importar')
    finally:
        conn.commit()

        # Chamada função de importação nas devidas tabelas, não importando quando já exisir um identificador na tabela
        validar_duplicidade(tabela)


def validar_duplicidade(tabela):
    """ Função responsável por transitar os dados da tabela stage para as respectivas tabelas dos métodos """

    query = f"""INSERT INTO [dbo].[{tabela}] SELECT * FROM [dbo].[VALIDACAO] WHERE [IDENTIFICADOR] NOT IN 
    (SELECT [IDENTIFICADOR] FROM [dbo].[{tabela}])"""
    cursor.execute(query)
    cursor.execute("TRUNCATE TABLE [dbo].[VALIDACAO]")
    conn.commit()
