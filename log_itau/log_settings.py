import logging
from datetime import datetime

path = 'C:/Users/lucas/Projetos/logitau/logs'

logging.root.setLevel(logging.NOTSET)
# Criar um logger customizado
logger = logging.getLogger(__name__)

# Criar handlers
handler = logging.FileHandler(f'{path}/{datetime.now().strftime("%d%m%Y")}.log')
handler.setLevel(logging.NOTSET)

# Criar formatters e adicionar nos handlers
format = logging.Formatter('%(asctime)s - %(levelname)s - %(message)s')
handler.setFormatter(format)

# Adicinar os hanflers nos loggers
logger.addHandler(handler)
