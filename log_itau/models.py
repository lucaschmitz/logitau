# Arquivo responsável pelo modelo do objeto

class LogDocumento:
    """ Classe responsável pela criação do objeto que reflete o layout do arquivo de log da LP """
    def __init__(self, dt_register, date, time, site_name, computer_name, s_ip, method, uri_stem, uri_query, port,
                 username, c_ip, version, user_agent, cookie, referer, status, sub_status, win32_status, time_taken,
                 identificador):
        """ Função reponsável pela inicialização do objeto """
        self.dt_register = dt_register
        self.date = date
        self.time = time
        self.site_name = site_name
        self.computer_name = computer_name
        self.s_ip = s_ip
        self.method = method
        self.uri_stem = uri_stem
        self.uri_query = uri_query
        self.port = port
        self.username = username
        self.c_ip = c_ip
        self.version = version
        self.user_agent = user_agent
        self.cookie = cookie
        self.referer = referer
        self.status = status
        self.sub_status = sub_status
        self.win32_status = win32_status
        self.time_taken = time_taken
        self.identificador = identificador

    def __str__(self):
        """ Representação em string do objeto """
        return f'{self.dt_register} {self.date} {self.time} {self.site_name} {self.computer_name} {self.s_ip}' \
               f'{self.method} {self.uri_stem} {self.uri_query} {self.port} {self.username} {self.c_ip}' \
               f'{self.version} {self.user_agent} {self.cookie} {self.referer} {self.status} {self.sub_status}' \
               f'{self.win32_status} {self.time_taken} {self.identificador}'
