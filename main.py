import os
import time
import shutil
from datetime import datetime
from log_itau.log_settings import logger
from log_itau.helpers import instanciar_arquivo, inserir_dados, objeto_para_tupla


def main():
    path = 'E:/LOGS_IIS/W3SVC31'
    for file in os.listdir(path):
        if file.endswith(".log"):
            file_path = f"{path}/{file}"
            data_ultima_modificacao_arquivo = datetime.fromtimestamp(os.stat(file_path).st_mtime)
            dados_get, dados_post, dados_options = [], [], []
            try:
                logger.info(f'{file} - Iniciando leitura arquivo')
                time.sleep(120)
                with open(file_path, 'r', encoding="latin1") as entrada:
                    contador_linhas = 0
                    for conteudo in entrada:
                        if conteudo[0:4].isnumeric() is True and len(conteudo.split()) == 19:
                            conteudo_objeto = instanciar_arquivo(conteudo.split(), f'{file}{contador_linhas}')
                            if conteudo_objeto.method == 'GET':
                                conteudo = objeto_para_tupla(conteudo_objeto)
                                dados_get.append(conteudo)
                            if conteudo_objeto.method == 'POST':
                                conteudo = objeto_para_tupla(conteudo_objeto)
                                dados_post.append(conteudo)
                            if conteudo_objeto.method == 'OPTIONS':
                                conteudo = objeto_para_tupla(conteudo_objeto)
                                dados_options.append(conteudo)
                            contador_linhas += 1
                    logger.info(f'{file} - Finalizando leitura arquivo')
            except PermissionError:
                logger.exception(f'{file} - Permissao negada para acessar o arquivo.')
            else:
                logger.info(f'{file} - Insercao dos dados na tabela GET')
                inserir_dados(tuple(dados_get), 'GET')

                logger.info(f'{file} - Insercao dos dados na tabela POST')
                inserir_dados(tuple(dados_post), 'POST')

                logger.info(f'{file} - Insercao dos dados na tabela OPTIONS')
                inserir_dados(tuple(dados_options), 'OPTIONS')

                if data_ultima_modificacao_arquivo.strftime('%d/%m/%Y') != datetime.now().strftime('%d/%m/%Y'):
                    logger.info(f'{file} - Movimentar arquivo para pasta OK')
                    new_path = f"{path}/OK/{file}"
                    logger.info(f'{file} - Mover aquivo para a pasta {new_path}')
                    shutil.move(f"{file_path}", f"{new_path}")
